package ru.zolotukhin.hockeyNHL;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    private Integer counterHomeTeam=0;
    private Integer counterVisitTeam=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    public void onClickGolHomeTeam(View view){
        counterHomeTeam++;
        TextView counterHome=(TextView) findViewById(R.id.accountHomeTeam);
        counterHome.setText(counterHomeTeam.toString());
        RelativeLayout relativeLayout;
        relativeLayout=(RelativeLayout) findViewById(R.id.backgroundProgramm);
        if(counterVisitTeam==counterHomeTeam){
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.startProgramm));
        }
        else if(counterHomeTeam<counterVisitTeam){
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorVisitTeam));
        }else{
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorHomeTeam));}
    }
    public void onClickGolVisitTeam(View view){
        counterVisitTeam++;
        TextView counterVisit=(TextView) findViewById(R.id.accountVisitTeam);
        counterVisit.setText(counterVisitTeam.toString());
        RelativeLayout relativeLayout;
        relativeLayout=(RelativeLayout) findViewById(R.id.backgroundProgramm);
        if(counterVisitTeam==counterHomeTeam){
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.startProgramm));
        } else if(counterVisitTeam<counterHomeTeam){
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorHomeTeam));
        }else{
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorVisitTeam));}

    }
}

